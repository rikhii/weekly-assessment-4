const areaOfTriangle = (base, height) => {
  return (base * height) / 2;
};

console.log(areaOfTriangle(3, 2)); // 3
console.log(areaOfTriangle(7, 4)); // 14
console.log(areaOfTriangle(10, 10)); // 50
